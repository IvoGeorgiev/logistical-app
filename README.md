# Logistics App Introduction
Logistics App - The application will be used by employees of a large Australian company aiming to expand its activities to the freight industry. The app will be used to manage the delivery of packages between hubs in major Australian cities



# Callable functions and their parameters:
- createpackage - Creates packages and checks if route for them is available, if not - marks them as unassigned packages - Expects 5 parameters :
createpackage StartLocation EndLocation Kilograms Surname Email

- createroute - Creates routes and assigns trucks to them - Expects at least 2 parameters :
createroute *At least 2 cities required

- assignpackage - Expectes 2 parameters, assigns the package ID to the Route ID if both parameters are correct, otherwise raises a Value Error:
assignpackage PackageID(1000001) RouteID(100001)

- unassignpackages - Unassign all packages from a given Route ID - Expects 1 parameter:
unassignpackages RouteID(100001)

- removeunassignedpackage - Deletes unassigned packages from the system - Expects 1 parameter:
removeunassignedpackage PackageID(1000001)

- searchroute - Expects 2 parameters (start and end location) in order to find a suitable route, if one is located, prints the details to the terminal, otherwise raises a Value Error:
searchroute StartLocation EndLocation

- routesinfo - Returns all routes information (ID, all locations and estimated dates and times of arrival) - No parameters required :
routesinfo *Nothing required

- packagesinfo - Returns all packages information(ID, start | end location of the package, weight, surname, email, route ID *if assigned to one) :
packagesinfo *Nothing required

- freetrucksinfo - Returns available trucks IDs - No parameters required :
freetrucksinfo *Nothing required

- busytrucksinfo - Returns busy trucks IDs - No parameters required :
busytrucksinfo *Nothing required

- deleteroute - Deletes routes - Expects 1 parameter:
deleteroute RouteID(1000001)

- findallinfo - Prints ALL information that has been collected by the system :
findallinfo *Nothing required



# Private functions which are not intended to be called outside of their classes:
- deleteroutesauto - Deletes all finished routes automatically - No parameters required :
*1 parameter required right now, for testing purposes (deleteroutesauto {number of days})
deleteroutesauto *Nothing required

- _assign_packages_auto - Checks if there are any unassigned packages and if there are suitable routes for them. Executes automatically at the end of the program. *Nothing required

- _return_truck_ToFree - Changes the availability of a truck(makes it unavailable for other routes) - whether a truck is already assigned to a route - Expects 1 parameter - Route ID

- _make_truck_busy - Changes the availability of a truck(makes it available for other routes) - whether a truck is already assigned to a route - Expects 1 parameter - Truck ID

- write_line - Writes data in save files - Expects 2 parameters - file_name, text(to be written on a new line)

- delete_line - Deletes data from save files - Expects 2 parameters - file_name, text(to identify the line that needs to be deleted)

- _id_syncer - Syncs the IDs of the routes and packages so that there are no differences between the IDs. Expects 1 parameter - Packages or Routes save file



# Planned functionality
- Add packages KGs to trucks to avoid overloading them

- Create file system

- Send customers an email when package arrives.

- Create user/admin system for managing or using the WHOLE system
