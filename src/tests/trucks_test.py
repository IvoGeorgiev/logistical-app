import unittest
from data.trucks import Trucks

class Constructor_Should(unittest.TestCase):
    def test_CreateValidName(self):
        
        # Arrange & Act
        truck = Trucks("Scania", 42000, 8000, 1001)

        # Assert
        self.assertEquals('Scania', truck.name)

    def test_RaiseError_NameInvalid(self):

        # Arrange, Act & Assert
        with self.assertRaises(ValueError):
            truck = Trucks("Scnia", 42000, 8000, 1001)
        