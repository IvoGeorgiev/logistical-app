import unittest
from data.packages import Packages


class PackagesShould(unittest.TestCase):
    def test_propertiesCorrect(self):

        # Arrange
        package = Packages("Sydney", "Brisbane", 45, "Someone", "email@sth.com")
        package2 = Packages("Sydney", "Brisbane", 452, "Someone", "email@sth.com")
        
        # Act & Assert
        self.assertEqual(package.email, "email@sth.com")
        self.assertEqual(package.name, "Someone")
        self.assertEqual(package.id, 1000001)
        self.assertEqual(package.weight, 45.0)
        self.assertEqual(package.all_info, "Sydney | Brisbane Weight: 45.0kgs Name: Someone Email: email@sth.com")

        self.assertEqual(package2.id, 1000002)

    def test_RaiseError_WeightInvalid(self):

        # Arrange & Act & Assert
        with self.assertRaises(ValueError):
            package = Packages("Sydney", "Brisbane", -45, "Someone", "email@sth.com")

    def test_RaiseError_EmailInvalid(self):
        
        # Arrange & Act & Assert
        with self.assertRaises(ValueError):
            package = Packages("Sydney", "Brisbane", 45, "Someone", "emailsth.com")
    
    def test_RaiseError_NameInvalid(self):
        
        # Arrange & Act & Assert
        with self.assertRaises(ValueError):
            package = Packages("Sydney", "Brisbane", 45, "@", "email@sth.com")