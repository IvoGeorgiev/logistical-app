import unittest

from data.application_data import ApplicationData


class ApplicationDataShould(unittest.TestCase):
    def test_InitializeVariableCorrectly(self):
       
        # Arrange
        sth = ApplicationData()

        # Act & Assert
        self.assertIsInstance(sth._all_packages, dict)
        self.assertIsInstance(sth._assigned_packages, dict)
        self.assertIsInstance(sth._unassigned_packages, dict)    
        self.assertIsInstance(sth._routes, dict)
        self.assertIsInstance(sth._busy_trucks, dict)        
        self.assertEquals(sth._free_trucks, [True] * 40) 

    def test_createPackageCorrectly(self):
       
        # Arrange
        sth = ApplicationData()

        # Act
        sth.create_package(["Sydney", "Brisbane", 45, "Someone", "email@sth.com"])

        # Assert
        for z, x in sth._all_packages.items():
            self.assertEquals(z, 1000001)
            self.assertEquals(x, "Sydney | Brisbane Weight: 45.0kgs Name: Someone Email: email@sth.com")
        
        for z, x in sth._unassigned_packages.items():
            self.assertEquals(z, 1000001)
            self.assertEquals(x, "Sydney | Brisbane Weight: 45.0kgs Name: Someone Email: email@sth.com")

    def test_createRouteCorrectly(self):
       
        # Arrange
        sth = ApplicationData()

        # Act
        sth.create_route(["31-12", "Sydney", "Brisbane"])

        # Assert
        for z, x in sth._routes.items():
            self.assertEquals(z, 100001)
            self.assertIsNotNone(x)
        
        for z, x in sth._busy_trucks.items():
            self.assertEquals(z, 1001)
            self.assertEquals(x, 100001)

    def test_assign_package_WorksCorrectly(self):
       
        # Arrange
        sth = ApplicationData()

        # Act
        sth.create_package(["Sydney", "Brisbane", 45, "Someone", "email@sth.com"])
        sth.create_route(["31-12", "Sydney", "Brisbane"])
        sth.assign_package(["1000001", "100001"])

        # Assert
        self.assertFalse(sth._unassigned_packages)
        for z, x in sth._assigned_packages.items():
            self.assertEquals(z, 1000001)
            self.assertEquals(x, 100001)

    def test_raiseError_PackageIDInvalid(self):
        
        # Arrange
        sth = ApplicationData()

        # Act & Assert
        with self.assertRaises(ValueError):
            sth.create_package(["Sydney", "Brisbane", 45, "Someone", "email@sth.com"])
            sth.create_route(["31-12", "Sydney", "Brisbane"])
            sth.assign_package(["100000", "100001"])

    def test_raiseError_RouteIDInvalid(self):
        
        # Arrange
        sth = ApplicationData()

        # Act & Assert
        with self.assertRaises(ValueError):
            sth.create_package(["Sydney", "Brisbane", 45, "Someone", "email@sth.com"])
            sth.create_route(["31-12", "Sydney", "Brisbane"])
            sth.assign_package(["1000001", "10000"])

    def test_delete_route_WorksCorrectly(self):

        # Arrange
        sth = ApplicationData()

        # Act
        sth.create_route(["31-12", "Sydney", "Brisbane"])
        self.assertTrue(sth._routes)
        sth.delete_route(["100001"])        

        # Assert
        self.assertFalse(sth._routes)
        self.assertEqual(sth._free_trucks[0], True)

    def test_delete_route_auto_WorksCorrectly(self):

        # Arrange
        sth = ApplicationData()

        # Act
        sth.create_route(["31-12", "Sydney", "Brisbane"])
        self.assertTrue(sth._routes)
        sth.delete_routes_auto(["250"])        

        # Assert
        self.assertFalse(sth._routes)
        self.assertEqual(sth._free_trucks[0], True)