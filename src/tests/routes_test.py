import unittest
from data.routes import Routes
from datetime import datetime, timedelta


class RoutesShould(unittest.TestCase):
    def test_propertiesCorrect(self):

        # Arrange
        route = Routes(["31-12", "Sydney", "Brisbane"])
        route2 = Routes(["31-12", "Sydney", "Melbourne"])
        
        # Act
        route.create_route()
         
        # Assert
        self.assertEqual(route.id, 100001)
        self.assertEqual(route._total_kms, 909)
        self.assertEqual(route._current_route, f"Sydney - Brisbane : {route._total_kms}kms - 2022.12.31 06:00h -> 2022.12.31 16:00h\n")
        self.assertIsInstance(route._locations, list)

        self.assertEqual(route2.id, 100002)

    def test_RaiseError_CitiesInvalid(self):

        # Arrange & Act & Assert
        with self.assertRaises(ValueError):
            route = Routes(["31-12", "Sydne", "Brisbane"])
            route.create_route()