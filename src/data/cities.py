class Cities:
    cities = ["Sydney", "Melbourne", "Adelaide", "Alice_Springs", "Brisbane", "Darwin", "Perth"]
    
    routes = {"Sydney - Melbourne": 877, "Sydney - Adelaide": 1376, "Sydney - Alice_Springs": 2762,
              "Sydney - Brisbane": 909, "Sydney - Darwin": 3965, "Sydney - Perth": 4016,
              "Melbourne - Sydney": 877, "Melbourne - Adelaide": 725, "Melbourne - Alice_Springs": 2255,
              "Melbourne - Brisbane": 1765, "Melbourne - Darwin": 3752, "Melbourne - Perth": 3509,
              "Adelaide - Sydney": 1376, "Adelaide - Melbourne": 725, "Adelaide - Alice_Springs": 1530,
              "Adelaide - Brisbane": 1927, "Adelaide - Darwin": 3027, "Adelaide - Perth": 2785,
              "Alice_Springs - Sydney": 2762, "Alice_Springs - Melbourne": 2255, "Alice_Springs - Adelaide": 1530,
              "Alice_Springs - Brisbane": 2993, "Alice_Springs - Perth": 2481, "Alice_Springs - Darwin": 1497,
              "Brisbane - Sydney": 909, "Brisbane - Melbourne": 1756, "Brisbane - Adelaide": 1927,
              "Brisbane - Alice_Springs": 2993, "Brisbane - Darwin": 3426, "Brisbane - Perth": 4311,
              "Darwin - Sydney": 3935, "Darwin - Melbourne": 3752, "Darwin - Adelaide": 3027,
              "Darwin - Alice_Springs": 1497, "Darwin - Brisbane": 3426, "Darwin - Perth": 4025,
              "Perth - Sydney": 4016, "Perth - Melbourne": 3509, "Perth - Adelaide": 3027,
              "Perth - Alice_Springs": 2481, "Perth - Brisbane": 4311, "Perth - Darwin": 4025}