from data.routes import Routes
from data.packages import Packages
from data.trucks import Trucks
from datetime import datetime
from os import path
import os
cwd = path.dirname(__file__)
unassigned_data = path.join(cwd, "../data_files/unassigned_packages_data.txt")
assigned_data = path.join(cwd, "../data_files/assigned_packages_data.txt")
all_data = path.join(cwd, "../data_files/all_packages_data.txt")
routes_data = path.join(cwd, "../data_files/routes_data.txt")
busy_trucks_data = path.join(cwd, "../data_files/busy_trucks_data.txt")
_free_trucks_data = path.join(cwd, "../data_files/_free_trucks_data.txt")


class ApplicationData:

    package_id = 1000001
    route_id = 100001

    def __init__(self):
        self._delete_routes_auto()
        self._id_syncer(routes_data)
        self._id_syncer(all_data)
        self._assign_packages_auto()

    def create_route(self, params):
        current_route = Routes(self.route_id, params)
        self.route_id += 1
        current_route.create_route()
        if current_route._total_kms > 13000:
            raise ValueError('Route is too long')
        print("Route added to system.")

        with open(_free_trucks_data, "r") as file:
            for z in file.readlines():
                x = z.split(" = ")
                id = int(x[0])
                status = x[1]
                if status == "True\n" and current_route._total_kms < 8000 and id - 1000 < 11:
                    truck = Trucks("Scania", 42000, 8000, id)
                    self._make_truck_busy(id)
                    break
                elif status == "True\n" and current_route._total_kms < 10000 and 10 < id - 1000 < 26:
                    truck = Trucks("Man", 37000, 10000, id)
                    self._make_truck_busy(id)
                    break
                elif status == "True\n" and current_route._total_kms < 13000 and 25 < id - 1000 < 41:
                    truck = Trucks("Actros", 26000, 13000, id)
                    self._make_truck_busy(id)
                    break
            else:
                print("No free trucks")
                return

        self.write_line(routes_data, f"{current_route.id} = {current_route._current_route}\n")  
        self.write_line(busy_trucks_data, f"{truck._id} = {current_route.id}\n")

    def create_package(self, params):
        current_package = Packages(self.package_id, params[0], params[1], params[2], params[3], params[4])
        self.package_id += 1
        route_id = self.search_route([params[0], params[1]])

        self.write_line(unassigned_data, f"{current_package.id} = {current_package.all_info}\n")
        print("Package added to system.")

        if route_id is not None:
            self.assign_package([current_package.id, route_id])

        self.write_line(all_data, f"{current_package.id} = {current_package.all_info}\n")

    def search_route(self, params):
        with open(routes_data, "r") as data:
            for z in data.readlines():
                if z == "\n": continue
                x = z.split(" = ")
                string = x[1].split(" ")
                if params[0] in string and params[1] in string:
                    city1 = string.index(params[0])
                    city2 = string.index(params[1])
                    if city1 < city2:
                        print(f"ID: {x[0]}  Route information: {x[1]}")
                        return x[0]

        print("No route available at the moment.")

    def assign_package(self, params):
        if str(params[0]) not in open(unassigned_data).read():
            raise ValueError("Invalid package ID.")
        if str(params[1]) not in open(routes_data).read():
            raise ValueError("Invalid route ID.")

        self.write_line(assigned_data, f"{params[0]} = {params[1]}\n")
        self.remove_unassigned_package([params[0]])
        print("Package assigned to route.")

    def unassign_packages(self, id):
        with open(assigned_data, "r") as file:
            for z in file.readlines():
                x = z.split(" = ")
                if z == "\n": continue
                if x[1] == f"{id[0]}\n":
                    with open(all_data, "r") as data:
                        for c in data.readlines():
                            if c == "\n": continue
                            if str(c.split(" = ")[0]) == str(x[0]):
                                self.write_line(unassigned_data, c)
                                self.delete_line(assigned_data, x[0])
                                break

    def remove_unassigned_package(self, param):
        if str(param[0]) not in open(unassigned_data).read():
            raise ValueError("Invalid package ID.")

        self.delete_line(unassigned_data, param[0])

    def _assign_packages_auto(self):
        with open(unassigned_data, "r") as file:
            if os.path.getsize(unassigned_data) == 0: return
            for z in file:
                if z != "\n":
                    x = z.split(" ")
                    start = x[2]
                    end = x[4]
                    route_id = self.search_route([start, end])
                    if route_id is not None:
                        self.assign_package([x[0], route_id])

    def _delete_routes_auto(self):
        with open(routes_data, "r") as data:
            for z in data.readlines():
                x = z.split(" = ")
                string = x[1].split()[-3]
                string += f" {x[1].split()[-2]}"
                time = datetime.strptime(string, "%Y.%m.%d %H:00h")
                if time < datetime.now():
                    self.delete_route([str(x[0])])

    def delete_route(self, param):
        self._return_truck_ToFree(param[0])
        self.unassign_packages([param[0]])

    def _return_truck_ToFree(self, id):
        with open(busy_trucks_data, "r+") as data:
            for z in data.readlines():
                if z == "\n": continue
                v = z.split(" = ")
                if v[1] == f"{str(id)}\n":
                    lines = open(_free_trucks_data, 'r').readlines()
                    lines[int(v[0]) - 1001] = f"{v[0]} = True\n"
                    out = open(_free_trucks_data, 'w')
                    out.writelines(lines)
                    out.close()
                    self.delete_line(routes_data, id)
                    self.delete_line(busy_trucks_data, id)
                    print(f"Route #{id} deleted.")
                    break

    def _make_truck_busy(self, id):
        lines = open(_free_trucks_data, 'r').readlines()
        lines[id - 1001] = f"{id} = False\n"
        out = open(_free_trucks_data, 'w')
        out.writelines(lines)
        out.close()

    def routes_info(self):
        string = "Routes:\n"
        with open(routes_data, "r") as data:
            for z in data.readlines():
                if z == "\n": continue
                x = z.split(" = ")
                string += f"ID: {x[0]} | Route information: -> {x[1]}"

        print(string)

    def packages_info(self):
        string = "Assigned packages:\n"
        with open(assigned_data, "r") as file:
            for z in file.readlines():
                if z == "\n": continue
                x = z.split(" = ")
                with open(all_data, "r") as data:
                    for c in data.readlines():
                        if c == "\n": continue
                        if str(c.split(" = ")[0]) == str(x[0]):
                            string += f"ID: {x[0]} | Package information: {c.split(' = ')[1]} - Route ID: {x[1]}\n"

        string += "Unassigned packages:"
        with open(unassigned_data, "r") as data:
            for z in data.readlines():
                if z == "\n": continue
                x = z.split(" = ")
                string += f"\nID: {x[0]} | Package information: {x[1]}"

        print(string)

    def free_trucks_info(self):
        print("Free trucks:")
        new_string = "-> Scania: "

        with open(_free_trucks_data, "r") as file:
            for z in file.readlines():
                x = z.split(" = ")
                if x[1] != "True\n":
                    continue
                if int(x[0]) == 1011:
                    new_string += "\n-> Man: "
                elif int(x[0]) == 1026:
                    new_string += "\n-> Actros: "
                new_string += str(int(x[0])) + " "

        print(new_string)

    def busy_trucks_info(self):
        string = "Busy trucks:"
        with open(busy_trucks_data, "r") as data:
            for z in data.readlines():
                if z == "\n": continue
                x = z.split(" = ")
                string += f" {x[0]}"

        print(string)

    def find_all_info(self):
        self.routes_info()
        self.packages_info()
        self.busy_trucks_info()
        self.free_trucks_info()

    def write_line(self, file_name, text):
        with open(file_name, "a") as file:
            file.write(text)

    def delete_line(self, file_name, text):
        with open(file_name, "r+") as file:
            lines = file.readlines()
            file.seek(0)
            for z in lines:
                if str(text) not in z and z != "\n":
                    file.write(z)
            file.truncate()

    def _id_syncer(self, txt):
        with open(txt, "r") as file:
            if os.path.getsize(txt) == 0: return
            for z in file:
                if z != "\n":
                    last_line = z
            x = int(last_line.split(" = ")[0])
            if txt is routes_data:
                self.route_id = x + 1
            else:
                self.package_id = x + 1
