class Trucks:
    def __init__(self, name, capacity, max_range, id):
        self.name = name
        self.capacity = capacity
        self.range = max_range
        self._id = id

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        names = ["Scania", "Man", "Actros"]
        if value not in names:
            raise ValueError("No such truck.")
        self._name = value
