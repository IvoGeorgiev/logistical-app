from datetime import timedelta, datetime
from data.cities import Cities


class Routes:
    def __init__(self, id, locations):
        self.date = locations[0]
        locations.pop(0)
        self._locations = locations
        self._total_kms = 0
        self._current_route = ""
        self.id = id

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        if value < 100001:
            raise ValueError
        self._id = value
        return self._id

    @property
    def date(self):
        return self._current_date

    @date.setter
    def date(self, value):
        starting_date = datetime.strptime(value, "%d-%m")
        starting_date = starting_date.replace(year=datetime.today().year, hour=6, minute=00)
        if starting_date < datetime.today():
            raise ValueError("Date has already passed.")
        self._current_date = starting_date

    def create_route(self):
        Routes.city_checker(self, self._locations)
        x = 0
        prev_time = self.date.strftime("%Y.%m.%d %H:00h")
        for z in self._locations:
            if x >= len(self._locations) - 1:
                break
            x += 1
            beg_end = z + " - " + self._locations[x]
            kms = Cities.routes[beg_end]
            time = Routes.calculate_time(self, kms, prev_time)
            self._total_kms += kms
            string = "".join(str(z) + " : " + str(x) + "kms - " + str(prev_time) + ' -> ' + str(time) for z, x in Cities.routes.items() if str(z) == beg_end)
            self._current_route += f"{string} ||| "
            prev_time = time

    def city_checker(self, cities):
        for z in cities:
            if z not in Cities.cities:
                raise ValueError("No such city in the system.")

    def calculate_time(self, kms, prev_time):
        time = kms // 87
        date = datetime.strptime(prev_time, "%Y.%m.%d %H:00h")
        ETA = date + timedelta(hours=time)
        formated_ETA = ETA.strftime("%Y.%m.%d %H:00h")
        return str(formated_ETA)
