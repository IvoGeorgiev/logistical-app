class Packages:

    def __init__(self, id, start, end, weight, surname, email):
        self._start = start
        self._end = end
        self.weight = weight
        self.surname = surname
        self.email = email
        self.all_info = f"{self._start} | {self._end} Weight: {self.weight}kgs Surname: {self.surname} Email: {self.email}"
        self.id = id

    @property
    def surname(self):
        return self._surname

    @surname.setter
    def surname(self, value):
        if not value.isalpha() or len(value) < 2:
            raise ValueError("Client's surname is invalid.")
        self._surname = value
        return self._surname

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, value):
        if "@" in value and len(value) > 5:
            self._email = value
            return self._email
        raise ValueError("Client's email address is invalid.")

    @property
    def weight(self):
        return self._weight

    @weight.setter
    def weight(self, value):
        if float(value) <= 0:
            raise ValueError("Package weight cannot be below 0.")
        self._weight = float(value)
        return self._weight

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        if value < 1000001:
            raise ValueError
        self._id = value
        return self._id
