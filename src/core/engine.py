from core.command_factory import CommandFactory


class Engine:
    def __init__(self, factory: CommandFactory):
        self._command_factory = factory

    def start(self):
        while True:
            try:
                input_line = input()
                if input_line.lower() == 'end':
                    break
                self._command_factory.create(input_line)
            except ValueError as err:
                print(err.args[0])
