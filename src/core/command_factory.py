from data.application_data import ApplicationData


class CommandFactory:
    def __init__(self, data: ApplicationData):
        self._data = data

    def create(self, input_line):
        cmd, *params = input_line.split()

        if cmd.lower() == "createpackage":
            ApplicationData.create_package(self._data, params)
        elif cmd.lower() == "createroute":
            return ApplicationData.create_route(self._data, params)
        elif cmd.lower() == "assignpackage":
            return ApplicationData.assign_package(self._data, params)
        elif cmd.lower() == "searchroute":
            return ApplicationData.search_route(self._data, params)
        elif cmd.lower() == "routesinfo":
            return ApplicationData.routes_info(self._data)
        elif cmd.lower() == "packagesinfo":
            return ApplicationData.packages_info(self._data)
        elif cmd.lower() == "freetrucksinfo":
            return ApplicationData.free_trucks_info(self._data)
        elif cmd.lower() == "busytrucksinfo": 
            return ApplicationData.busy_trucks_info(self._data)
        elif cmd.lower() == "deleteroute":
            return ApplicationData.delete_route(self._data, params)    
        elif cmd.lower() == "findallinfo":
            return ApplicationData.find_all_info(self._data)
        else:
            raise ValueError(f"Invalid command name: {cmd}!")
